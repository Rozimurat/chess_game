import java.util.Scanner;
import java.util.Vector;

public class Board {

    Vector <Piece> elements = new Vector<>();
    {
        Rook br1 = new Rook(true, new Position("a8")); elements.add(br1);
        Rook br2 = new Rook(true,  new Position("h8")); elements.add(br2);

        Horse bh1 = new Horse(true, new Position("b8")); elements.add(bh1);
        Horse bh2 = new Horse(true, new Position("g8")); elements.add(bh2);

        Elephant be1 = new Elephant(true, new Position("c8")); elements.add(be1);
        Elephant be2 = new Elephant(true, new Position("f8")); elements.add(be2);

        King bk = new King(true, new Position("e8")); elements.add(bk);
        Queen bq = new Queen(true, new Position("d8"));  elements.add(bq);

        for (int i = 97; i <97+8; i++) {
            String huh = "";
            huh += (char)i;
            huh += '7';
            Pawn pawn = new Pawn(true, new Position(huh));
            elements.add(pawn);
        }
        Rook wr1 = new Rook(false,new Position("a1"));			elements.add(wr1);
        Rook wr2 = new Rook(false, new Position("h1"));			elements.add(wr2);

        Horse wh1 = new Horse(false,new Position("b1"));		 	elements.add(wh1);
        Horse wh2 = new Horse(false, new Position("g1"));			elements.add(wh2);

        Elephant we1  = new Elephant(false, new Position("c1"));		elements.add(we1);
        Elephant we2 = new Elephant(false, new Position("f1"));		elements.add(we2);

        King wk = new King(false, new Position("e1"));				elements.add(wk);
        Queen wq = new Queen(false, new Position("d1"));				elements.add(wq);

        for(int i = 97 ; i<= 8+97 ; i++ ) {   //PAWNS
            String huh = "";
            huh += (char)i;
            huh+='2';
            elements.add(new Pawn(false, new Position(huh)));
        }
    }

    public void showGame(){
        System.out.print("   a  b  c  d  e  f  g  h\n");
        for (int i = 0; i < 8; i++) {
            System.out.print(8-i);
            System.out.print(" ");

            out:for (int j = 0; j < 8; j++) {
                for (Piece p : elements) {
                    if(p.position.getX() == i && p.position.getY() == j){
                        System.out.print("[" + p + "]");
                        continue out;
                    }
                }
                System.out.print("[ ]");
            }
            System.out.print("\n");
        }
    }

    boolean isRunning(){
        int black = 0;
        int white = 0;
        for (Piece p : elements) {
            if(p.isBlack){
                black += 1;
            }else {
                white += 1;
            }
        }
        return white > 0  && black > 0;
    }

    void nextPlayer(boolean isBlack){
        String s = isBlack ? "black" : "white";
        System.out.println("Go, ahead, " + s+ "\n");
    }

    void eaten(Piece p){
        System.out.println("oops...you ate it!\n ");
        elements.remove(p);
    }


    void makeMove(Piece piece, Position pos, boolean isBlack){
        if(piece.isBlack != isBlack) System.out.println("Not your color spacy place...\n");

        else {
            if(piece.isLegalMove(pos)){
                for (Piece checkPiece : elements){
                    if(checkPiece.position.equals(pos) && piece.isBlack != checkPiece.isBlack){
                        eaten(checkPiece);  // eat enemy;
                        break;
                    }
                    piece.changePosition(pos);  //else just go there...
                }
            }
            else {
                System.out.println("You gonna make such a mistake move..I mean illegal move:\n");
            }
        }
    }

    Piece findByPosition(Position p){
        for (Piece piece: elements) {
            if(piece.position.equals(p)){
                return piece;
            }
        }
        return new Pawn(true, new Position("z9"));
    }

    void showWinner(){
        String s = "";
        for (Piece p : elements){
            s = p.isBlack ? "black":"white";
            break;
        }
        System.out.println("WINNER IS..." + s);
    }

    void game(){
        Scanner scanner = new Scanner(System.in);
        boolean isBlack = true;
        while(isRunning()){
            showGame();

            nextPlayer(isBlack);
            System.out.print("Enter position of piece you want to move: ");
            String from = scanner.next();
            System.out.print("Enter position of where you want it to move: ");
            String go = scanner.next();
            makeMove(findByPosition(new Position(from)), new Position(go), findByPosition(new Position(from)).isBlack);
            isBlack = isBlack ? false : true; // just turn of next player
        }
        scanner.close();
        showWinner();
    }



}
