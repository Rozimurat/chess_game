public class Rook extends Piece{
    Rook(boolean isBlack, Position position) {
        super(isBlack, position);
    }

    @Override
    public boolean isLegalMove(Position b) {
        if (position.getX() == b.getX() || position.getY() == b.getY() && isLegalMove(this.position, b)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return isBlack? "♜" : "♖" ;
    }

}
