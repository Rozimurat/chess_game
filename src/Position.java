import java.util.Objects;

public class Position {
    private int x;
    private int y;

    Position(String s){
        setX(8 - ((int) (s.charAt(1) - 48))); // X is digit (1-8)
        setY((int)(s.charAt(0) - 97));  //letter is Y
    }
    public int getX() {
        return x;
    }


    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return getX() == position.getX() && getY() == position.getY();
    }

    boolean inBorder(){
        return getX() >=0 && getX() <= 8 && getY() >= 0 && getY() <= 8;
    }

}
