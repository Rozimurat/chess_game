public class Queen extends Piece{
    Queen(boolean isBlack, Position position) {
        super(isBlack, position);
    }

    @Override
    public boolean isLegalMove(Position b) {
        Elephant e = new Elephant(this.isBlack, this.position);
        Rook r = new Rook(this.isBlack, this.position);
        if(e.isLegalMove(b) || r.isLegalMove(b)){
            return true;
        }

        return false;
    }


    @Override
    public String toString() {
        return isBlack? "♚" : "♔";
    }
}
